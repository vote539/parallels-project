#include <iostream>
#include <math.h>
#include "mex.h"
#include "matrix.h"
#include "fft.h"

void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[]){
    
    // Check sizes to prevent segfaults
    if (nrhs != 2) {
        mexErrMsgIdAndTxt( "MATLAB:fft:nargin",
                "First argument must be an array. Second argument must "
                "be a boolean indicating whether to perform inverse FFT.");
    }
    if (nlhs != 1) {
        mexErrMsgIdAndTxt("MATLAB:fft:nargout",
                "One output argument is required.");
    }
    
    // Get dimensions of input matrix
    const int ndims = mxGetNumberOfDimensions(prhs[0]);
    const int* dims = mxGetDimensions(prhs[0]);
    if (ndims < 1 || ndims > 3) {
        mexErrMsgIdAndTxt("MATLAB:fft:ndims",
                "Only 1D, 2D, and 3D matrices are supported.");
    }
    
    int lmn = 1;
    for (int i=0; i<ndims; i++) lmn *= dims[i];
    
    // Make output matrix
    plhs[0] = mxCreateNumericArray(ndims, dims, mxDOUBLE_CLASS, mxCOMPLEX);
    
    // Make pointers
    const double* in_real_ptr = mxGetPr(prhs[0]);
    const double* in_imag_ptr = mxGetPi(prhs[0]);
    const double in_inv = mxGetScalar(prhs[1]); 
    double* out_real_ptr = mxGetPr(plhs[0]);
    double* out_imag_ptr = mxGetPi(plhs[0]);
    
    // Copy to array format compatible with FFT library
    COMPLEX* xx = new COMPLEX[lmn];
    COMPLEX* res = new COMPLEX[lmn];
    double real;
    double imag = 0.0;
    for (size_t i=0; i<lmn; i++) {
        real = in_real_ptr[i];
        if (in_imag_ptr != NULL) 
            imag = out_real_ptr[i];
        xx[i] = COMPLEX(real, imag);
    }
    
    // Run algorithm
    if (ndims == 1 && in_inv > 0) {
        ifft1d(xx, res, dims[0]);
    } else if (ndims == 1) {
        fft1d(xx, res, dims[0]);
    } else if (ndims == 2 && in_inv > 0) {
        ifft2d(xx, res, dims[1], dims[0]);
    } else if (ndims == 2) {
        fft2d(xx, res, dims[1], dims[0]);
    } else if (ndims == 3 && in_inv > 0) {
        ifft3d(xx, res, dims[2], dims[1], dims[0]);
    } else {
        fft3d(xx, res, dims[2], dims[1], dims[0]);
    }
    
    // Copy to output
    for (size_t i=0; i<lmn; i++) {
        out_real_ptr[i] = res[i].real();
        out_imag_ptr[i] = res[i].imag();
    }
    
    return;
}