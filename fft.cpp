//
//  fft.cpp
//  
//
//  Created by Grant Schmadeke on 12/5/14.
//
//

#include "fft.h"

#include <iostream>
#include <random>
#include <chrono>
#include <ctime>

double print_chrono_duration(chrono::duration<double, milli> durr){
	return durr.count();
}

int main(int argc, char **args) {
	size_t L, M, N;
	if (argc >= 4){
		L = strtol(args[1], NULL, 0);
		M = strtol(args[2], NULL, 0);
		N = strtol(args[3], NULL, 0);
	}
	else{
		cout << "Could not read arguments, defaulting to (20,30,40)" << endl;
		L = 20;
		M = 30;
		N = 40;
	}
	size_t LM = L*M;
	size_t LMN = LM*N;

	COMPLEX* nums = new COMPLEX[LMN];
	COMPLEX* res = new COMPLEX[LMN];

	unsigned seed = 10;
	minstd_rand0 gen(seed);
	uniform_real_distribution<double> dis(0, 10);

	for (size_t i = 0; i < LMN; i++){
		nums[i] = (COMPLEX)dis(gen);
	}

	auto t1 = chrono::steady_clock::now();

	fft1d(nums, res, LMN);

	auto t2 = chrono::steady_clock::now();
	cout << "FFT 1D Time: " << print_chrono_duration(t2 - t1) << " ms" << endl;

	ifft1d(nums, res, LMN);

	auto t3 = chrono::steady_clock::now();
	cout << "Inverse FFT 1D Time: " << print_chrono_duration(t3 - t2) << " ms" << endl;

	fft2d(nums, res, LM, N);

	auto t4 = chrono::steady_clock::now();
	cout << "FFT 2D Time: " << print_chrono_duration(t4 - t3) << " ms" << endl;

	ifft2d(nums, res, LM, N);

	auto t5 = chrono::steady_clock::now();
	cout << "Inverse FFT 2D Time: " << print_chrono_duration(t5 - t4) << " ms" << endl;

	fft3d(nums, res, L, M, N);

	auto t6 = chrono::steady_clock::now();
	cout << "FFT 3D Time: " << print_chrono_duration(t6 - t5) << " ms" << endl;

	ifft3d(nums, res, L, M, N);

	auto t7 = chrono::steady_clock::now();
	cout << "Inverse FFT 3D Time: " << print_chrono_duration(t7 - t6) << " ms" << endl;

	return 0;
}
