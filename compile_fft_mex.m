% Build the MATLAB binding for the FFT algorithm

if isunix
    % Compile for Cilk Plus on Linux
    mex -v -O -lcilkrts -DCILKIFY CXXFLAGS='$CXXFLAGS -fcilkplus' ...
        fft_mex.cpp fft1d.cpp fft2d.cpp fft3d.cpp transpose.cpp
else
    % Compile normally on Windows
    mex -v -O fft_mex.cpp fft1d.cpp fft2d.cpp fft3d.cpp transpose.cpp
end