// Header file for the FFT implementation.

// Dependencies
using namespace std;
#include <complex>
#include <cmath>
#include <cstdlib>

// Cilk Plus dependencies
#ifdef CILKIFY
#include <cilk/cilk.h>
#include <cilk/cilk_api.h>
#include <cilk/reducer_opadd.h>
#else
#define cilk_for for
#endif

// Shortcut data types and constants
#define COMPLEX complex<double>
const double pi = acos(-1);
const COMPLEX IMAGINARY(0, 1);

// UTILITY FUNCTIONS

// Transpose the MxN matrix `mat` into the NxM matrix` tr`.
void transpose(COMPLEX* mat, COMPLEX* tr, size_t M, size_t N);

// Transpose, but touch the elements of `mat` and `tr` at intervals of `skip`.
void transpose(COMPLEX* mat, COMPLEX* tr, size_t M, size_t N, size_t skip);

// CORE FFT FUNCTIONS
// Perform the 1d, 2d, or 3d FFT on `x`, saving the answer in `ret`.
// The functions starting with "i" perform the inverse FFT.
// The input and output arrays are stored in row-major order.
void fft1d(COMPLEX* x, COMPLEX* ret, size_t N);
void ifft1d(COMPLEX* x, COMPLEX* ret, size_t N);
void fft2d(COMPLEX* x, COMPLEX* ret, size_t M, size_t N);
void ifft2d(COMPLEX* x, COMPLEX* ret, size_t M, size_t N);
void fft3d(COMPLEX* x, COMPLEX* ret, size_t L, size_t M, size_t N);
void ifft3d(COMPLEX* x, COMPLEX* ret, size_t L, size_t M, size_t N);
