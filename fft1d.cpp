#include "fft.h"
#include <cstring>

/* Compute a twiddle factor in constant time.
 * 
 * Returns:  e^( 2 pi i k / N)  for the forward transform
 *      or:  e^(-2 pi i k / N)  for the inverse transform
 */
COMPLEX twiddle(size_t k, size_t N, bool inv){
	return exp((inv ? 2 : -2) * pi * IMAGINARY *
		((double)k) / ((double)N));
}

/* Naive FFT implementation
 *
 * Computes the Discrete Fourier Transform of x, saving it into m2,
 * using the definition of the DFT in O(N^2) work.
 * 
 * Other arguments:
 *   N => the length of the array being transformed
 *   fctr => a number equal to or lower than the lowest prime factor of N
 *   inv => whether to perform the inverse FFT rather than the forward FFT
 */
void fft1d_naive(COMPLEX* x, COMPLEX* ret, const size_t N,
	size_t skip, bool inv) {

	cilk_for(size_t i=0; i<N; i++){
		size_t is = i * skip;

#ifdef CILKIFY
		cilk::reducer< cilk::op_add<COMPLEX> > sum_reducer(0.0);
		cilk_for(size_t j=0; j<N; j++){
			size_t js = j * skip;
			COMPLEX w = x[js] * pow(twiddle(i, N, inv), j);
			*sum_reducer += w;
		}
		ret[is] = sum_reducer.get_value();
#else
		COMPLEX sum = 0.0;
		for (size_t j = 0; j<N; j++){
			size_t js = j * skip;
			COMPLEX w = x[js] * pow(twiddle(i, N, inv), j);
			sum += w;
		}
		ret[is] = sum;
#endif

		// Remove imaginary part if it is small
		if(abs(real(ret[i]) / imag(ret[i])) > 1000000.0){
			ret[i] = real(ret[i]);
		}
	}
}

/* Main worker function
 * 
 * Computes the Discrete Fourier Transform of m1, saving it into m2,
 * using the Cooley-Tukey method in O(N lg N) work.
 * 
 * NOTE: Array m1 will be changed.
 * 
 * Other arguments:
 *   N => the length of the array being transformed
 *   skip => in the input array, look at elements at increments of skip
 *   fctr => a number equal to or lower than the lowest prime factor of N
 *   inv => whether to perform the inverse FFT rather than the forward FFT
 */
void fft1d_cooley_tukey(COMPLEX* m1, COMPLEX* m2, const size_t N,
	size_t skip, size_t fctr, bool inv) {

	// Attempt to find the next prime factor of N
#ifdef MAXRAD
	int s = (int) sqrt(N);
	while (N % s != 0) {
		s--;
	}
#else
	while (N % fctr != 0) {
		fctr++;
	}
#endif

	// Base case: fall back to naive algorithm
#ifdef MAXRAD
	if (s == 1) {
#else
	if (N == fctr){
#endif
		fft1d_naive(m1, m2, N, skip, inv);
		return;
	}

	// Name variables to be consistent with Cooley Tukey algorithm
	size_t P = N / fctr;
	size_t Q = fctr;

	// Recurse First Time
	cilk_for(size_t q = 0; q<Q; q++){
		fft1d_cooley_tukey(m1 + q*skip, m2 + q*skip, P, skip*Q, fctr, inv);
	}

	// Apply Twiddles
	cilk_for(size_t q = 0; q<Q; q++){
		cilk_for(size_t p = 0; p<P; p++){
			size_t retidx = (p*Q + q)*skip;
			size_t twdl_idx = q*p;
			COMPLEX twdl = twiddle(twdl_idx, N, inv);

			m2[retidx] *= twdl;
		}
	}

	// Recurse Second Time
	cilk_for(size_t p = 0; p<P; p++){
		fft1d_cooley_tukey(m2 + p*Q*skip, m1 + p*Q*skip, Q, skip, fctr, inv);
	}

	// Transpose Matrix
	transpose(m1, m2, P, Q, skip);
}

// PUBLIC FUNCTIONS (see docs in the header file)

void fft1d(COMPLEX* x, COMPLEX* ret, size_t N) {
	// Since the worker function modifies the first argument, copy the input
	// to this function into a new temporary array.
	COMPLEX* tmp = new COMPLEX[N];
	memcpy(tmp, x, N*sizeof(COMPLEX));

	// Run the algorithm
	fft1d_cooley_tukey(tmp, ret, N, 1, 2, false);

	// Release the memory
	delete[] tmp;
}

void ifft1d(COMPLEX* x, COMPLEX* ret, size_t N) {
	// Since the worker function modifies the first argument, copy the input
	// to this function into a new temporary array.
	COMPLEX* tmp = new COMPLEX[N];
	memcpy(tmp, x, N*sizeof(COMPLEX));

	// Run the algorithm
	fft1d_cooley_tukey(tmp, ret, N, 1, 2, true);

	// Release the memory
	delete[] tmp;

	// The inverse FFT requires that we divide all elements by N.  Do that now.
	cilk_for (size_t i = 0; i<N; i++){
		ret[i] /= ((double)N);
	}
}
