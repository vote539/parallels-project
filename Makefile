CC := g++
CFLAGS := -Wall -O2 -g -std=c++11 -fcilkplus -DCILKIFY=1
LIBS := -lcilkrts

SOURCES := fft.cpp fft1d.cpp fft2d.cpp fft3d.cpp transpose.cpp
HEADERS := *.h
OBJS := $(SOURCES:.cpp=.o)

all: fft

clean:
	rm -rf *.o

run: fft
	./fft

check: fft
	./fft | diff - correct1.txt

fft: $(OBJS)
	$(CC) $(CFLAGS) -o fft $(OBJS) $(LFLAGS) $(LIBS)

.cpp.o: $(HEADERS)
	$(CC) $(CFLAGS) $(INCLUDES) -c $<
