#include "fft.h"

void transpose(COMPLEX* mat, COMPLEX* tr, size_t M, size_t N){
	cilk_for(size_t i = 0; i < M; i++){
		cilk_for(size_t k = 0; k < N; k++){
			tr[M*k + i] = mat[N*i + k];
		}
	}
}

void transpose(COMPLEX* mat, COMPLEX* tr, size_t M, size_t N, size_t skip){
	cilk_for(size_t i = 0; i < M; i++){
		cilk_for(size_t k = 0; k < N; k++){
			tr[(M*k + i)*skip] = mat[(N*i + k)*skip];
		}
	}
}
