#include "fft.h"

/* Worker function to perform the 2D Discrete Fourier Transform.
 *
 * Arrays `x` and `ret` are both assumed to be MxN matrices stored in
 * row-major order.
 */
void fft2d(COMPLEX* x, COMPLEX* ret, const size_t M, const size_t N, bool inv) {
	// Allocate two extra arrays
	COMPLEX* m1 = new COMPLEX[M*N];
	COMPLEX* m2 = new COMPLEX[M*N];

	// Perform FFT on the columns
	transpose(x, m1, M, N);
	cilk_for(size_t i = 0; i < N; i++){
		(inv ? ifft1d : fft1d)(m1 + M*i, m2 + M*i, M);
	}

	// Transpose back
	transpose(m2, m1, N, M);

	// Perform FFT on the rows
	cilk_for(size_t i = 0; i < M; i++){
		(inv ? ifft1d : fft1d)(m1 + N*i, ret + N*i, N);
	}

	// Relieve memory
	delete[] m1;
	delete[] m2;
}

// PUBLIC FUNCTIONS (see docs in the header file)
void fft2d(COMPLEX* x, COMPLEX* ret, size_t M, size_t N){
	fft2d(x, ret, M, N, false);
}
void ifft2d(COMPLEX* x, COMPLEX* ret, size_t M, size_t N){
	fft2d(x, ret, M, N, true);
}