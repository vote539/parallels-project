#include "fft.h"

/* Worker function to perform the 3D Discrete Fourier Transform.
*
* Arrays `x` and `ret` are both assumed to be LxMxN matrices stored in
* row-major order.
*/
void fft3d(COMPLEX* x, COMPLEX* ret, const size_t L, const size_t M, const size_t N, bool inv) {
	COMPLEX* m1 = new COMPLEX[L*M*N];
	COMPLEX* m2 = new COMPLEX[L*M*N];

	// Perform FFT on the rows
	cilk_for(size_t i = 0; i < L*M; i++){
		(inv ? ifft1d : fft1d)(x + N*i, m2 + N*i, N);
	}

	// Transpose the array to make columns adjacent to one another
	cilk_for(size_t i = 0; i < L; i++){
		transpose(m2 + i*M*N, m1 + i*M*N, M, N);
	}

	// Perform FFT on the columns
	cilk_for(size_t i = 0; i < L*N; i++){
		(inv ? ifft1d : fft1d)(m1 + M*i, m2 + M*i, M);
	}

	// Transpose the array to make layers adjacent to one another
	transpose(m2, m1, L, M*N);

	// Perform FFT on the layers (third dimension)
	cilk_for(size_t i = 0; i < M*N; i++){
		(inv ? ifft1d : fft1d)(m1 + L*i, m2 + L*i, L);
	}

	// Reverse the transpositions
	transpose(m2, m1, M*N, L);
	cilk_for(size_t i = 0; i < L; i++){
		transpose(m1 + i*M*N, ret + i*M*N, N, M);
	}

	// Relieve memory
	delete[] m1;
	delete[] m2;
}

// PUBLIC FUNCTIONS (see docs in the header file)
void fft3d(COMPLEX* x, COMPLEX* ret, size_t L, size_t M, size_t N){
	fft3d(x, ret, L, M, N, false);
}
void ifft3d(COMPLEX* x, COMPLEX* ret, size_t L, size_t M, size_t N){
	fft3d(x, ret, L, M, N, true);
}
